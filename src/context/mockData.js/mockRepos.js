export default [
  {
    id: 293454605,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTM0NTQ2MDU=',
    name: 'Amazon-Price-Tracker',
    full_name: 'marco714/Amazon-Price-Tracker',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Amazon-Price-Tracker',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Amazon-Price-Tracker',
    forks_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/teams',
    hooks_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Amazon-Price-Tracker/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Amazon-Price-Tracker/deployments',
    created_at: '2020-09-07T07:31:19Z',
    updated_at: '2020-09-07T07:46:14Z',
    pushed_at: '2020-09-07T07:46:11Z',
    git_url: 'git://github.com/marco714/Amazon-Price-Tracker.git',
    ssh_url: 'git@github.com:marco714/Amazon-Price-Tracker.git',
    clone_url: 'https://github.com/marco714/Amazon-Price-Tracker.git',
    svn_url: 'https://github.com/marco714/Amazon-Price-Tracker',
    homepage: null,
    size: 4744,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 297967693,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTc5Njc2OTM=',
    name: 'Chat-Application',
    full_name: 'marco714/Chat-Application',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Chat-Application',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Chat-Application',
    forks_url: 'https://api.github.com/repos/marco714/Chat-Application/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Chat-Application/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Chat-Application/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Chat-Application/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Chat-Application/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Chat-Application/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Chat-Application/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Chat-Application/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Chat-Application/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Chat-Application/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Chat-Application/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Chat-Application/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Chat-Application/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Chat-Application/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Chat-Application/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Chat-Application/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Chat-Application/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Chat-Application/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Chat-Application/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Chat-Application/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Chat-Application/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Chat-Application/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Chat-Application/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Chat-Application/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Chat-Application/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Chat-Application/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Chat-Application/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Chat-Application/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Chat-Application/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Chat-Application/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Chat-Application/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Chat-Application/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Chat-Application/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Chat-Application/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Chat-Application/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Chat-Application/deployments',
    created_at: '2020-09-23T12:47:21Z',
    updated_at: '2020-09-23T12:47:21Z',
    pushed_at: '2020-09-23T12:47:23Z',
    git_url: 'git://github.com/marco714/Chat-Application.git',
    ssh_url: 'git@github.com:marco714/Chat-Application.git',
    clone_url: 'https://github.com/marco714/Chat-Application.git',
    svn_url: 'https://github.com/marco714/Chat-Application',
    homepage: null,
    size: 0,
    stargazers_count: 0,
    watchers_count: 0,
    language: null,
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 303331563,
    node_id: 'MDEwOlJlcG9zaXRvcnkzMDMzMzE1NjM=',
    name: 'Chat_Application',
    full_name: 'marco714/Chat_Application',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Chat_Application',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Chat_Application',
    forks_url: 'https://api.github.com/repos/marco714/Chat_Application/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Chat_Application/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Chat_Application/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Chat_Application/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Chat_Application/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Chat_Application/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Chat_Application/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Chat_Application/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Chat_Application/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Chat_Application/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Chat_Application/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Chat_Application/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Chat_Application/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Chat_Application/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Chat_Application/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Chat_Application/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Chat_Application/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Chat_Application/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Chat_Application/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Chat_Application/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Chat_Application/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Chat_Application/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Chat_Application/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Chat_Application/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Chat_Application/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Chat_Application/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Chat_Application/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Chat_Application/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Chat_Application/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Chat_Application/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Chat_Application/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Chat_Application/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Chat_Application/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Chat_Application/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Chat_Application/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Chat_Application/deployments',
    created_at: '2020-10-12T08:47:52Z',
    updated_at: '2020-10-12T08:51:21Z',
    pushed_at: '2020-10-12T08:51:19Z',
    git_url: 'git://github.com/marco714/Chat_Application.git',
    ssh_url: 'git@github.com:marco714/Chat_Application.git',
    clone_url: 'https://github.com/marco714/Chat_Application.git',
    svn_url: 'https://github.com/marco714/Chat_Application',
    homepage: null,
    size: 21,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 308262794,
    node_id: 'MDEwOlJlcG9zaXRvcnkzMDgyNjI3OTQ=',
    name: 'CountryAPI',
    full_name: 'marco714/CountryAPI',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/CountryAPI',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/CountryAPI',
    forks_url: 'https://api.github.com/repos/marco714/CountryAPI/forks',
    keys_url: 'https://api.github.com/repos/marco714/CountryAPI/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/CountryAPI/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/CountryAPI/teams',
    hooks_url: 'https://api.github.com/repos/marco714/CountryAPI/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/CountryAPI/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/CountryAPI/events',
    assignees_url:
      'https://api.github.com/repos/marco714/CountryAPI/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/CountryAPI/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/CountryAPI/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/CountryAPI/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/CountryAPI/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/CountryAPI/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/CountryAPI/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/CountryAPI/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/marco714/CountryAPI/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/CountryAPI/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/CountryAPI/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/CountryAPI/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/CountryAPI/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/CountryAPI/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/CountryAPI/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/CountryAPI/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/CountryAPI/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/CountryAPI/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/CountryAPI/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/CountryAPI/merges',
    archive_url:
      'https://api.github.com/repos/marco714/CountryAPI/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/marco714/CountryAPI/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/CountryAPI/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/CountryAPI/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/CountryAPI/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/CountryAPI/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/CountryAPI/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/CountryAPI/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/CountryAPI/deployments',
    created_at: '2020-10-29T08:26:45Z',
    updated_at: '2020-10-29T08:28:55Z',
    pushed_at: '2020-10-29T08:28:52Z',
    git_url: 'git://github.com/marco714/CountryAPI.git',
    ssh_url: 'git@github.com:marco714/CountryAPI.git',
    clone_url: 'https://github.com/marco714/CountryAPI.git',
    svn_url: 'https://github.com/marco714/CountryAPI',
    homepage: null,
    size: 5,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 517650853,
    node_id: 'R_kgDOHtq5pQ',
    name: 'Covid19-TrackerApp',
    full_name: 'marco714/Covid19-TrackerApp',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Covid19-TrackerApp',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Covid19-TrackerApp',
    forks_url: 'https://api.github.com/repos/marco714/Covid19-TrackerApp/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Covid19-TrackerApp/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Covid19-TrackerApp/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Covid19-TrackerApp/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Covid19-TrackerApp/deployments',
    created_at: '2022-07-25T12:12:04Z',
    updated_at: '2022-10-19T05:51:23Z',
    pushed_at: '2022-07-25T12:20:45Z',
    git_url: 'git://github.com/marco714/Covid19-TrackerApp.git',
    ssh_url: 'git@github.com:marco714/Covid19-TrackerApp.git',
    clone_url: 'https://github.com/marco714/Covid19-TrackerApp.git',
    svn_url: 'https://github.com/marco714/Covid19-TrackerApp',
    homepage: 'covid19tracker-narca.vercel.app',
    size: 93,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 297609743,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTc2MDk3NDM=',
    name: 'Database',
    full_name: 'marco714/Database',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Database',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Database',
    forks_url: 'https://api.github.com/repos/marco714/Database/forks',
    keys_url: 'https://api.github.com/repos/marco714/Database/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Database/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Database/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Database/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Database/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Database/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Database/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Database/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Database/tags',
    blobs_url: 'https://api.github.com/repos/marco714/Database/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Database/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Database/git/refs{/sha}',
    trees_url: 'https://api.github.com/repos/marco714/Database/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Database/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/marco714/Database/languages',
    stargazers_url: 'https://api.github.com/repos/marco714/Database/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Database/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Database/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Database/subscription',
    commits_url: 'https://api.github.com/repos/marco714/Database/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Database/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Database/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Database/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Database/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Database/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Database/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Database/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/marco714/Database/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Database/issues{/number}',
    pulls_url: 'https://api.github.com/repos/marco714/Database/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Database/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Database/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/marco714/Database/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Database/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Database/deployments',
    created_at: '2020-09-22T10:07:56Z',
    updated_at: '2020-09-22T10:10:22Z',
    pushed_at: '2020-09-22T10:10:20Z',
    git_url: 'git://github.com/marco714/Database.git',
    ssh_url: 'git@github.com:marco714/Database.git',
    clone_url: 'https://github.com/marco714/Database.git',
    svn_url: 'https://github.com/marco714/Database',
    homepage: null,
    size: 11,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 290201818,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTAyMDE4MTg=',
    name: 'Ebay-Tracker',
    full_name: 'marco714/Ebay-Tracker',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Ebay-Tracker',
    description: 'Web-Scraping Ebay',
    fork: false,
    url: 'https://api.github.com/repos/marco714/Ebay-Tracker',
    forks_url: 'https://api.github.com/repos/marco714/Ebay-Tracker/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Ebay-Tracker/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Ebay-Tracker/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Ebay-Tracker/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Ebay-Tracker/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Ebay-Tracker/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Ebay-Tracker/deployments',
    created_at: '2020-08-25T11:54:27Z',
    updated_at: '2020-08-25T11:59:27Z',
    pushed_at: '2020-08-25T11:58:57Z',
    git_url: 'git://github.com/marco714/Ebay-Tracker.git',
    ssh_url: 'git@github.com:marco714/Ebay-Tracker.git',
    clone_url: 'https://github.com/marco714/Ebay-Tracker.git',
    svn_url: 'https://github.com/marco714/Ebay-Tracker',
    homepage: null,
    size: 7,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master',
  },
  {
    id: 289677659,
    node_id: 'MDEwOlJlcG9zaXRvcnkyODk2Nzc2NTk=',
    name: 'Final-Project',
    full_name: 'marco714/Final-Project',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Final-Project',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Final-Project',
    forks_url: 'https://api.github.com/repos/marco714/Final-Project/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Final-Project/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Final-Project/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Final-Project/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Final-Project/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Final-Project/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Final-Project/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Final-Project/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Final-Project/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Final-Project/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Final-Project/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Final-Project/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Final-Project/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Final-Project/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Final-Project/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Final-Project/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Final-Project/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Final-Project/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Final-Project/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Final-Project/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Final-Project/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Final-Project/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Final-Project/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Final-Project/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Final-Project/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Final-Project/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Final-Project/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Final-Project/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Final-Project/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Final-Project/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Final-Project/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Final-Project/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Final-Project/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Final-Project/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Final-Project/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Final-Project/deployments',
    created_at: '2020-08-23T12:03:52Z',
    updated_at: '2020-09-04T05:46:14Z',
    pushed_at: '2020-09-04T05:46:12Z',
    git_url: 'git://github.com/marco714/Final-Project.git',
    ssh_url: 'git@github.com:marco714/Final-Project.git',
    clone_url: 'https://github.com/marco714/Final-Project.git',
    svn_url: 'https://github.com/marco714/Final-Project',
    homepage: null,
    size: 260,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master',
  },
  {
    id: 298819698,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTg4MTk2OTg=',
    name: 'Flask-Session',
    full_name: 'marco714/Flask-Session',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Flask-Session',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Flask-Session',
    forks_url: 'https://api.github.com/repos/marco714/Flask-Session/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Flask-Session/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Flask-Session/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Flask-Session/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Flask-Session/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Flask-Session/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Flask-Session/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Flask-Session/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Flask-Session/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Flask-Session/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Flask-Session/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Flask-Session/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Flask-Session/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Flask-Session/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Flask-Session/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Flask-Session/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Flask-Session/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Flask-Session/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Flask-Session/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Flask-Session/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Flask-Session/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Flask-Session/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Flask-Session/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Flask-Session/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Flask-Session/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Flask-Session/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Flask-Session/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Flask-Session/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Flask-Session/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Flask-Session/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Flask-Session/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Flask-Session/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Flask-Session/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Flask-Session/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Flask-Session/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Flask-Session/deployments',
    created_at: '2020-09-26T13:25:53Z',
    updated_at: '2020-09-26T13:25:53Z',
    pushed_at: '2020-09-26T13:25:55Z',
    git_url: 'git://github.com/marco714/Flask-Session.git',
    ssh_url: 'git@github.com:marco714/Flask-Session.git',
    clone_url: 'https://github.com/marco714/Flask-Session.git',
    svn_url: 'https://github.com/marco714/Flask-Session',
    homepage: null,
    size: 0,
    stargazers_count: 0,
    watchers_count: 0,
    language: null,
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 485278663,
    node_id: 'R_kgDOHOzDxw',
    name: 'Heroku_Demo',
    full_name: 'marco714/Heroku_Demo',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Heroku_Demo',
    description: 'This is a repository for my demo',
    fork: false,
    url: 'https://api.github.com/repos/marco714/Heroku_Demo',
    forks_url: 'https://api.github.com/repos/marco714/Heroku_Demo/forks',
    keys_url: 'https://api.github.com/repos/marco714/Heroku_Demo/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Heroku_Demo/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Heroku_Demo/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Heroku_Demo/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Heroku_Demo/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Heroku_Demo/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Heroku_Demo/deployments',
    created_at: '2022-04-25T08:07:20Z',
    updated_at: '2022-04-25T08:12:51Z',
    pushed_at: '2022-04-25T08:12:48Z',
    git_url: 'git://github.com/marco714/Heroku_Demo.git',
    ssh_url: 'git@github.com:marco714/Heroku_Demo.git',
    clone_url: 'https://github.com/marco714/Heroku_Demo.git',
    svn_url: 'https://github.com/marco714/Heroku_Demo',
    homepage: null,
    size: 1,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 301921156,
    node_id: 'MDEwOlJlcG9zaXRvcnkzMDE5MjExNTY=',
    name: 'IntegrateFastAPI',
    full_name: 'marco714/IntegrateFastAPI',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/IntegrateFastAPI',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/IntegrateFastAPI',
    forks_url: 'https://api.github.com/repos/marco714/IntegrateFastAPI/forks',
    keys_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/IntegrateFastAPI/teams',
    hooks_url: 'https://api.github.com/repos/marco714/IntegrateFastAPI/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/IntegrateFastAPI/events',
    assignees_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/IntegrateFastAPI/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/IntegrateFastAPI/merges',
    archive_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/IntegrateFastAPI/deployments',
    created_at: '2020-10-07T03:56:14Z',
    updated_at: '2020-10-07T03:57:24Z',
    pushed_at: '2020-10-07T03:57:22Z',
    git_url: 'git://github.com/marco714/IntegrateFastAPI.git',
    ssh_url: 'git@github.com:marco714/IntegrateFastAPI.git',
    clone_url: 'https://github.com/marco714/IntegrateFastAPI.git',
    svn_url: 'https://github.com/marco714/IntegrateFastAPI',
    homepage: null,
    size: 9,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 295094932,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTUwOTQ5MzI=',
    name: 'InvoiceAutomation',
    full_name: 'marco714/InvoiceAutomation',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/InvoiceAutomation',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/InvoiceAutomation',
    forks_url: 'https://api.github.com/repos/marco714/InvoiceAutomation/forks',
    keys_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/InvoiceAutomation/teams',
    hooks_url: 'https://api.github.com/repos/marco714/InvoiceAutomation/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/events',
    assignees_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/InvoiceAutomation/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/merges',
    archive_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/InvoiceAutomation/deployments',
    created_at: '2020-09-13T06:47:20Z',
    updated_at: '2020-09-13T09:04:07Z',
    pushed_at: '2020-09-13T09:04:05Z',
    git_url: 'git://github.com/marco714/InvoiceAutomation.git',
    ssh_url: 'git@github.com:marco714/InvoiceAutomation.git',
    clone_url: 'https://github.com/marco714/InvoiceAutomation.git',
    svn_url: 'https://github.com/marco714/InvoiceAutomation',
    homepage: null,
    size: 1,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 295111741,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTUxMTE3NDE=',
    name: 'InvoiceAutomator',
    full_name: 'marco714/InvoiceAutomator',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/InvoiceAutomator',
    description:
      "🧾 Let's automate Invoice generation from CSV file (@jakobowsky YouTube tutorial)",
    fork: true,
    url: 'https://api.github.com/repos/marco714/InvoiceAutomator',
    forks_url: 'https://api.github.com/repos/marco714/InvoiceAutomator/forks',
    keys_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/InvoiceAutomator/teams',
    hooks_url: 'https://api.github.com/repos/marco714/InvoiceAutomator/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/InvoiceAutomator/events',
    assignees_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/InvoiceAutomator/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/InvoiceAutomator/merges',
    archive_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/InvoiceAutomator/deployments',
    created_at: '2020-09-13T08:43:45Z',
    updated_at: '2020-09-13T08:43:47Z',
    pushed_at: '2020-09-12T14:38:55Z',
    git_url: 'git://github.com/marco714/InvoiceAutomator.git',
    ssh_url: 'git@github.com:marco714/InvoiceAutomator.git',
    clone_url: 'https://github.com/marco714/InvoiceAutomator.git',
    svn_url: 'https://github.com/marco714/InvoiceAutomator',
    homepage: '',
    size: 138,
    stargazers_count: 0,
    watchers_count: 0,
    language: null,
    has_issues: false,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 275779522,
    node_id: 'MDEwOlJlcG9zaXRvcnkyNzU3Nzk1MjI=',
    name: 'Java-Example',
    full_name: 'marco714/Java-Example',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Java-Example',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Java-Example',
    forks_url: 'https://api.github.com/repos/marco714/Java-Example/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Java-Example/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Java-Example/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Java-Example/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Java-Example/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Java-Example/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Java-Example/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Java-Example/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Java-Example/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Java-Example/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Java-Example/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Java-Example/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Java-Example/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Java-Example/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Java-Example/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Java-Example/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Java-Example/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Java-Example/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Java-Example/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Java-Example/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Java-Example/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Java-Example/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Java-Example/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Java-Example/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Java-Example/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Java-Example/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Java-Example/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Java-Example/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Java-Example/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Java-Example/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Java-Example/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Java-Example/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Java-Example/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Java-Example/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Java-Example/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Java-Example/deployments',
    created_at: '2020-06-29T08:51:16Z',
    updated_at: '2020-06-30T07:49:55Z',
    pushed_at: '2020-06-30T07:49:53Z',
    git_url: 'git://github.com/marco714/Java-Example.git',
    ssh_url: 'git@github.com:marco714/Java-Example.git',
    clone_url: 'https://github.com/marco714/Java-Example.git',
    svn_url: 'https://github.com/marco714/Java-Example',
    homepage: null,
    size: 11,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'PowerShell',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 301288211,
    node_id: 'MDEwOlJlcG9zaXRvcnkzMDEyODgyMTE=',
    name: 'JavaScript_Project',
    full_name: 'marco714/JavaScript_Project',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/JavaScript_Project',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/JavaScript_Project',
    forks_url: 'https://api.github.com/repos/marco714/JavaScript_Project/forks',
    keys_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/JavaScript_Project/teams',
    hooks_url: 'https://api.github.com/repos/marco714/JavaScript_Project/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/events',
    assignees_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/JavaScript_Project/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/merges',
    archive_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/JavaScript_Project/deployments',
    created_at: '2020-10-05T03:58:25Z',
    updated_at: '2020-10-05T03:59:38Z',
    pushed_at: '2020-10-05T03:59:36Z',
    git_url: 'git://github.com/marco714/JavaScript_Project.git',
    ssh_url: 'git@github.com:marco714/JavaScript_Project.git',
    clone_url: 'https://github.com/marco714/JavaScript_Project.git',
    svn_url: 'https://github.com/marco714/JavaScript_Project',
    homepage: null,
    size: 12,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 300480605,
    node_id: 'MDEwOlJlcG9zaXRvcnkzMDA0ODA2MDU=',
    name: 'OnlineShop',
    full_name: 'marco714/OnlineShop',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/OnlineShop',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/OnlineShop',
    forks_url: 'https://api.github.com/repos/marco714/OnlineShop/forks',
    keys_url: 'https://api.github.com/repos/marco714/OnlineShop/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/OnlineShop/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/OnlineShop/teams',
    hooks_url: 'https://api.github.com/repos/marco714/OnlineShop/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/OnlineShop/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/OnlineShop/events',
    assignees_url:
      'https://api.github.com/repos/marco714/OnlineShop/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/OnlineShop/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/OnlineShop/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/OnlineShop/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/OnlineShop/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/OnlineShop/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/OnlineShop/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/OnlineShop/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/marco714/OnlineShop/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/OnlineShop/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/OnlineShop/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/OnlineShop/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/OnlineShop/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/OnlineShop/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/OnlineShop/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/OnlineShop/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/OnlineShop/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/OnlineShop/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/OnlineShop/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/OnlineShop/merges',
    archive_url:
      'https://api.github.com/repos/marco714/OnlineShop/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/marco714/OnlineShop/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/OnlineShop/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/OnlineShop/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/OnlineShop/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/OnlineShop/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/OnlineShop/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/OnlineShop/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/OnlineShop/deployments',
    created_at: '2020-10-02T02:29:24Z',
    updated_at: '2020-10-02T02:32:46Z',
    pushed_at: '2020-10-02T02:32:44Z',
    git_url: 'git://github.com/marco714/OnlineShop.git',
    ssh_url: 'git@github.com:marco714/OnlineShop.git',
    clone_url: 'https://github.com/marco714/OnlineShop.git',
    svn_url: 'https://github.com/marco714/OnlineShop',
    homepage: null,
    size: 197,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 275127245,
    node_id: 'MDEwOlJlcG9zaXRvcnkyNzUxMjcyNDU=',
    name: 'Sample-repo',
    full_name: 'marco714/Sample-repo',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Sample-repo',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Sample-repo',
    forks_url: 'https://api.github.com/repos/marco714/Sample-repo/forks',
    keys_url: 'https://api.github.com/repos/marco714/Sample-repo/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Sample-repo/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Sample-repo/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Sample-repo/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Sample-repo/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Sample-repo/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Sample-repo/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Sample-repo/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Sample-repo/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Sample-repo/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Sample-repo/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Sample-repo/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Sample-repo/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Sample-repo/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Sample-repo/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Sample-repo/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Sample-repo/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Sample-repo/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Sample-repo/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Sample-repo/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Sample-repo/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Sample-repo/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Sample-repo/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Sample-repo/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Sample-repo/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Sample-repo/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Sample-repo/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Sample-repo/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Sample-repo/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Sample-repo/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Sample-repo/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Sample-repo/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Sample-repo/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Sample-repo/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Sample-repo/deployments',
    created_at: '2020-06-26T10:13:08Z',
    updated_at: '2020-08-23T09:01:54Z',
    pushed_at: '2020-08-23T09:32:35Z',
    git_url: 'git://github.com/marco714/Sample-repo.git',
    ssh_url: 'git@github.com:marco714/Sample-repo.git',
    clone_url: 'https://github.com/marco714/Sample-repo.git',
    svn_url: 'https://github.com/marco714/Sample-repo',
    homepage: null,
    size: 1,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 296535446,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTY1MzU0NDY=',
    name: 'Semi-FinalProject',
    full_name: 'marco714/Semi-FinalProject',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Semi-FinalProject',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Semi-FinalProject',
    forks_url: 'https://api.github.com/repos/marco714/Semi-FinalProject/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Semi-FinalProject/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Semi-FinalProject/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Semi-FinalProject/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Semi-FinalProject/deployments',
    created_at: '2020-09-18T06:38:53Z',
    updated_at: '2020-09-18T12:01:10Z',
    pushed_at: '2020-09-18T12:01:08Z',
    git_url: 'git://github.com/marco714/Semi-FinalProject.git',
    ssh_url: 'git@github.com:marco714/Semi-FinalProject.git',
    clone_url: 'https://github.com/marco714/Semi-FinalProject.git',
    svn_url: 'https://github.com/marco714/Semi-FinalProject',
    homepage: null,
    size: 1,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 293458498,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTM0NTg0OTg=',
    name: 'Simple-Face-Detection',
    full_name: 'marco714/Simple-Face-Detection',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Simple-Face-Detection',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Simple-Face-Detection',
    forks_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/forks',
    keys_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/teams',
    hooks_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Simple-Face-Detection/deployments',
    created_at: '2020-09-07T07:48:21Z',
    updated_at: '2020-09-07T07:51:25Z',
    pushed_at: '2020-09-07T07:51:22Z',
    git_url: 'git://github.com/marco714/Simple-Face-Detection.git',
    ssh_url: 'git@github.com:marco714/Simple-Face-Detection.git',
    clone_url: 'https://github.com/marco714/Simple-Face-Detection.git',
    svn_url: 'https://github.com/marco714/Simple-Face-Detection',
    homepage: null,
    size: 133,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 289675571,
    node_id: 'MDEwOlJlcG9zaXRvcnkyODk2NzU1NzE=',
    name: 'Snake-Game',
    full_name: 'marco714/Snake-Game',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/Snake-Game',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/Snake-Game',
    forks_url: 'https://api.github.com/repos/marco714/Snake-Game/forks',
    keys_url: 'https://api.github.com/repos/marco714/Snake-Game/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/Snake-Game/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/Snake-Game/teams',
    hooks_url: 'https://api.github.com/repos/marco714/Snake-Game/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/Snake-Game/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/Snake-Game/events',
    assignees_url:
      'https://api.github.com/repos/marco714/Snake-Game/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/Snake-Game/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/Snake-Game/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/Snake-Game/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/Snake-Game/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/Snake-Game/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/Snake-Game/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/Snake-Game/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/marco714/Snake-Game/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/Snake-Game/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/Snake-Game/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/Snake-Game/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/Snake-Game/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/Snake-Game/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/Snake-Game/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/Snake-Game/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/Snake-Game/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/Snake-Game/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/Snake-Game/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/Snake-Game/merges',
    archive_url:
      'https://api.github.com/repos/marco714/Snake-Game/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/marco714/Snake-Game/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/Snake-Game/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/Snake-Game/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/Snake-Game/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/Snake-Game/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/Snake-Game/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/Snake-Game/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/Snake-Game/deployments',
    created_at: '2020-08-23T11:50:16Z',
    updated_at: '2020-08-25T11:59:27Z',
    pushed_at: '2020-08-23T11:52:32Z',
    git_url: 'git://github.com/marco714/Snake-Game.git',
    ssh_url: 'git@github.com:marco714/Snake-Game.git',
    clone_url: 'https://github.com/marco714/Snake-Game.git',
    svn_url: 'https://github.com/marco714/Snake-Game',
    homepage: null,
    size: 2,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master',
  },
  {
    id: 289662978,
    node_id: 'MDEwOlJlcG9zaXRvcnkyODk2NjI5Nzg=',
    name: 'test_',
    full_name: 'marco714/test_',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/test_',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/test_',
    forks_url: 'https://api.github.com/repos/marco714/test_/forks',
    keys_url: 'https://api.github.com/repos/marco714/test_/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/test_/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/test_/teams',
    hooks_url: 'https://api.github.com/repos/marco714/test_/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/test_/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/test_/events',
    assignees_url:
      'https://api.github.com/repos/marco714/test_/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/test_/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/test_/tags',
    blobs_url: 'https://api.github.com/repos/marco714/test_/git/blobs{/sha}',
    git_tags_url: 'https://api.github.com/repos/marco714/test_/git/tags{/sha}',
    git_refs_url: 'https://api.github.com/repos/marco714/test_/git/refs{/sha}',
    trees_url: 'https://api.github.com/repos/marco714/test_/git/trees{/sha}',
    statuses_url: 'https://api.github.com/repos/marco714/test_/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/marco714/test_/languages',
    stargazers_url: 'https://api.github.com/repos/marco714/test_/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/test_/contributors',
    subscribers_url: 'https://api.github.com/repos/marco714/test_/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/test_/subscription',
    commits_url: 'https://api.github.com/repos/marco714/test_/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/test_/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/test_/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/test_/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/test_/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/test_/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/test_/merges',
    archive_url:
      'https://api.github.com/repos/marco714/test_/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/marco714/test_/downloads',
    issues_url: 'https://api.github.com/repos/marco714/test_/issues{/number}',
    pulls_url: 'https://api.github.com/repos/marco714/test_/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/test_/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/test_/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/marco714/test_/labels{/name}',
    releases_url: 'https://api.github.com/repos/marco714/test_/releases{/id}',
    deployments_url: 'https://api.github.com/repos/marco714/test_/deployments',
    created_at: '2020-08-23T10:24:11Z',
    updated_at: '2020-08-23T11:32:07Z',
    pushed_at: '2020-08-23T11:32:10Z',
    git_url: 'git://github.com/marco714/test_.git',
    ssh_url: 'git@github.com:marco714/test_.git',
    clone_url: 'https://github.com/marco714/test_.git',
    svn_url: 'https://github.com/marco714/test_',
    homepage: null,
    size: 4,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
  {
    id: 308264514,
    node_id: 'MDEwOlJlcG9zaXRvcnkzMDgyNjQ1MTQ=',
    name: 'WebScraping',
    full_name: 'marco714/WebScraping',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/WebScraping',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/WebScraping',
    forks_url: 'https://api.github.com/repos/marco714/WebScraping/forks',
    keys_url: 'https://api.github.com/repos/marco714/WebScraping/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/WebScraping/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/WebScraping/teams',
    hooks_url: 'https://api.github.com/repos/marco714/WebScraping/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/WebScraping/issues/events{/number}',
    events_url: 'https://api.github.com/repos/marco714/WebScraping/events',
    assignees_url:
      'https://api.github.com/repos/marco714/WebScraping/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/WebScraping/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/WebScraping/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/WebScraping/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/WebScraping/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/WebScraping/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/WebScraping/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/WebScraping/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/WebScraping/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/WebScraping/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/WebScraping/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/WebScraping/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/WebScraping/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/WebScraping/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/WebScraping/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/WebScraping/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/WebScraping/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/WebScraping/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/WebScraping/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/marco714/WebScraping/merges',
    archive_url:
      'https://api.github.com/repos/marco714/WebScraping/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/WebScraping/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/WebScraping/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/WebScraping/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/WebScraping/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/WebScraping/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/WebScraping/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/WebScraping/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/WebScraping/deployments',
    created_at: '2020-10-29T08:34:29Z',
    updated_at: '2020-10-29T08:35:38Z',
    pushed_at: '2020-10-29T08:35:34Z',
    git_url: 'git://github.com/marco714/WebScraping.git',
    ssh_url: 'git@github.com:marco714/WebScraping.git',
    clone_url: 'https://github.com/marco714/WebScraping.git',
    svn_url: 'https://github.com/marco714/WebScraping',
    homepage: null,
    size: 7995,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'HTML',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'main',
  },
  {
    id: 298950427,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTg5NTA0Mjc=',
    name: 'WebServiceCrawler',
    full_name: 'marco714/WebServiceCrawler',
    private: false,
    owner: {
      login: 'marco714',
      id: 67462436,
      node_id: 'MDQ6VXNlcjY3NDYyNDM2',
      avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/marco714',
      html_url: 'https://github.com/marco714',
      followers_url: 'https://api.github.com/users/marco714/followers',
      following_url:
        'https://api.github.com/users/marco714/following{/other_user}',
      gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/marco714/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
      organizations_url: 'https://api.github.com/users/marco714/orgs',
      repos_url: 'https://api.github.com/users/marco714/repos',
      events_url: 'https://api.github.com/users/marco714/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/marco714/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/marco714/WebServiceCrawler',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/marco714/WebServiceCrawler',
    forks_url: 'https://api.github.com/repos/marco714/WebServiceCrawler/forks',
    keys_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/marco714/WebServiceCrawler/teams',
    hooks_url: 'https://api.github.com/repos/marco714/WebServiceCrawler/hooks',
    issue_events_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/events',
    assignees_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/branches{/branch}',
    tags_url: 'https://api.github.com/repos/marco714/WebServiceCrawler/tags',
    blobs_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/languages',
    stargazers_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/stargazers',
    contributors_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/contributors',
    subscribers_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/subscribers',
    subscription_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/subscription',
    commits_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/merges',
    archive_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/downloads',
    issues_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/labels{/name}',
    releases_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/marco714/WebServiceCrawler/deployments',
    created_at: '2020-09-27T04:07:16Z',
    updated_at: '2020-09-27T04:08:38Z',
    pushed_at: '2020-09-27T04:08:36Z',
    git_url: 'git://github.com/marco714/WebServiceCrawler.git',
    ssh_url: 'git@github.com:marco714/WebServiceCrawler.git',
    clone_url: 'https://github.com/marco714/WebServiceCrawler.git',
    svn_url: 'https://github.com/marco714/WebServiceCrawler',
    homepage: null,
    size: 0,
    stargazers_count: 0,
    watchers_count: 0,
    language: 'Python',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: 'master',
  },
];
