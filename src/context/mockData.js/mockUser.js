export default {
  login: 'marco714',
  id: 67462436,
  node_id: 'MDQ6VXNlcjY3NDYyNDM2',
  avatar_url: 'https://avatars.githubusercontent.com/u/67462436?v=4',
  gravatar_id: '',
  url: 'https://api.github.com/users/marco714',
  html_url: 'https://github.com/marco714',
  followers_url: 'https://api.github.com/users/marco714/followers',
  following_url: 'https://api.github.com/users/marco714/following{/other_user}',
  gists_url: 'https://api.github.com/users/marco714/gists{/gist_id}',
  starred_url: 'https://api.github.com/users/marco714/starred{/owner}{/repo}',
  subscriptions_url: 'https://api.github.com/users/marco714/subscriptions',
  organizations_url: 'https://api.github.com/users/marco714/orgs',
  repos_url: 'https://api.github.com/users/marco714/repos',
  events_url: 'https://api.github.com/users/marco714/events{/privacy}',
  received_events_url: 'https://api.github.com/users/marco714/received_events',
  type: 'User',
  site_admin: false,
  name: 'Marco Louis M. Narca',
  company: null,
  blog: '',
  location: null,
  email: null,
  hireable: null,
  bio: 'Hello Mars',
  twitter_username: null,
  public_repos: 23,
  public_gists: 0,
  followers: 0,
  following: 1,
  created_at: '2020-06-26T09:51:41Z',
  updated_at: '2022-10-19T05:43:31Z',
};
