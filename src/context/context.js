import React, { useState, useEffect, useContext } from 'react';
import mockUser from './mockData.js/mockUser';
import mockRepos from './mockData.js/mockRepos';
import mockFollowers from './mockData.js/mockFollowers';
import axios from 'axios';

const rootUrl = 'https://api.github.com';

const GithubContext = React.createContext();

const GithubProvider = ({ children }) => {
  const [githubUser, setGithubUser] = useState(mockUser);
  const [repos, setRepos] = useState(mockRepos);
  const [followers, setFollowers] = useState(mockFollowers);
  const [request, setRequest] = useState(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState({ show: false, msg: '' });

  const searchGithubUser = async (user) => {
    // Toggle error
    toggleError();
    // set loading
    setLoading(true);
    try {
      const response = await axios(`${rootUrl}/users/${user}`);

      if (response) {
        setGithubUser(response.data);
        const { login, followers_url } = response.data;
        let repos = await axios(`${rootUrl}/users/${login}/repos?per_page=100`);

        let followers = await axios(`${followers_url}?per_page=100`);

        [repos, followers] = await Promise.allSettled([repos, followers]);

        if (repos.status === 'fulfilled') {
          setRepos(repos.value.data);
        }

        if (followers.status === 'fulfilled') {
          setFollowers(followers.value.data);
        }
      }

      checkRequest();
      setLoading(false);
    } catch (error) {
      toggleError(true, 'There is no user with that username');
      setLoading(false);
      checkRequest();
      console.log(error);
    }
  };

  const checkRequest = async () => {
    try {
      const response = await axios(`${rootUrl}/rate_limit`);
      let {
        data: {
          rate: { remaining },
        },
      } = response;

      setRequest(remaining);
      if (remaining === 0) {
        toggleError(true, 'sorry, you have exceeded hourly rate limit');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const toggleError = (show = false, msg = '') => {
    setError({ show, msg });
  };

  useEffect(() => {
    checkRequest();
  }, [githubUser]);

  return (
    <GithubContext.Provider
      value={{
        githubUser,
        repos,
        followers,
        request,
        error,
        searchGithubUser,
        loading,
      }}
    >
      {children}
    </GithubContext.Provider>
  );
};

// const useGlobalContext = () => {
//   const data = useContext(GithubContext);
//   return data;
// };

export { GithubProvider, GithubContext };
